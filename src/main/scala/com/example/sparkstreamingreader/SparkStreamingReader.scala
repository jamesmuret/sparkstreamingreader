package com.example.sparkstreamingreader

import scala.math.random
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.StructType


/** Computes an approximation to pi */
object SparkStreamingReader {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .appName("Spark Streaming read")
      .master("local[*]")
      .getOrCreate()

    val bookSchema = new StructType()
      .add("Name", "string")
      .add("Author", "string")
      .add("Rating", "double")
      .add("Reviews", "long")
      .add("Price", "double")
      .add("Year", "int")
      .add("Genre", "string")

  val kafkaReadStreamDf =  spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", "localhost:29093")
      .option("subscribe", "books")
      .option("startingOffsets", "earliest")
      .option("value.deserializer", "org.apache.kafka.common.serialization.StringSerializer").load()
      .select("value").toJSON


    // фильтр записей по условию рейтинга
    val BookFilteredDF = kafkaReadStreamDf.select(from_json(col("value"), bookSchema).as("data"))
     .select("data.*").filter("Rating > 4")

    // без наличия стрима записи не запустится
    println("writeKafka JSON ToConsole")
    //kafkaReadStreamDf.writeStream.format("console").start().awaitTermination()


    BookFilteredDF.writeStream.queryName("writeKafkaToParquet").outputMode("append").format("parquet").option("path", "/user/root/Ln_sink2").option("checkpointLocation", "/user/root/Checkpoints2").start()
    spark.stop()
  }
}